//
//  QuestionData.swift
//  PersonalQuiz
//
//  Created by Michele Navolio on 12/12/2019.
//  Copyright © 2019 Michele Navolio. All rights reserved.
//

import UIKit

struct Question {
    var text : String
    var type : ResponseType
    var answers : [Answer]
}

enum ResponseType {
    case single, multiple, ranged
}

struct Answer {
    var text : String
    var type : AnimaleType
}

enum AnimaleType : Character {
    case dog = "🐶", cat = "🐱", rabbit = "🐰", turtle = "🐢"
    
    var definition : String {
        switch self {
        case .dog:
            return "You are incredibly outgoing. You surround yourself with the peolple you love and enjoy activities with your friends."
        case .cat:
            return "Mischievous, yet mild-tempered, you enjoy doing things on your own terms."
        case .rabbit:
            return "You love everything that's soft. You are healthy and full of energy."
        case .turtle:
            return "You are wise beyond your years, and you fiocus on the details. Slow and steady wins the race."
        }
    }
}
