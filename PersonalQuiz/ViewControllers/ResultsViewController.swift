//
//  ResultsViewController.swift
//  PersonalQuiz
//
//  Created by Michele Navolio on 12/12/2019.
//  Copyright © 2019 Michele Navolio. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController {
    
    @IBOutlet var resultAnswerLabel: UILabel!
    @IBOutlet var resultDefinitionLabel: UILabel!
    
    var responses: [Answer]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        calculatePersonalityResult()
        
        navigationItem.hidesBackButton = true
    }
    
    func calculatePersonalityResult() {
        
        var frequencyOfAnswers : [AnimaleType : Int] = [:]
        
        let responseType = responses.map { $0.type }
        
        for response in responseType {
            let newCount : Int
            
            if let oldCount = frequencyOfAnswers[response] {
                newCount = oldCount + 1
            } else {
                newCount = 1
            
            }
            frequencyOfAnswers[response] = newCount
            
        }
        
        let frequentAnswersSorted = frequencyOfAnswers.sorted(by: {
            (pair1, pair2) -> Bool in
            return pair1.value > pair2.value
        })
        
        let mostCommonAnswer = frequentAnswersSorted.first!.key

        resultAnswerLabel.text = "You are a \(mostCommonAnswer.rawValue)!"
        resultDefinitionLabel.text = mostCommonAnswer.definition
    }
    
    
    
    
}
